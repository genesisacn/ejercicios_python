#####Ejercicios de lección 4-6

####Lección 4 Funciones (def)

#Ejercicio1 

def distance_from_zero(n):
  if type(n) == int or type(n) == float:
    return abs(n)
  else:
    return "Nope"
 
print distance_from_zero((-5)*(15))

#Ejercicio2

def hotel_cost(nights):
  return 140 * nights

def plane_ride_cost(city):
  if city == "Charlotte":
    return 183
  elif city == "Tampa":
    return 220
  elif city == "Pittsburgh":
    return 222
  elif city == "Los Angeles":
    return 475

def rental_car_cost(days):
  cost = days * 40
  if days >= 7:
    cost -= 50
  elif days >= 3:
    cost -= 20
  return cost
##print rental_car_cost(3)

def trip_cost(city, days, spending_money):
  return rental_car_cost(days) + hotel_cost(days - 1) + plane_ride_cost(city) + spending_money

print trip_cost("Los Angeles", 5, 600)

####Lección 5 Listas y Diccionarios

#Ejercicio 1

zoo_animals = ["pangolin", "cassowary", "sloth", "cat"];

if len(zoo_animals) > 3:
  print "The first animal at the zoo is the " + zoo_animals[0]
  print "The second animal at the zoo is the " + zoo_animals[1]
  print "The third animal at the zoo is the " + zoo_animals[2]
  print "The fourth animal at the zoo is the " + zoo_animals[3]

#Ejercicio 2

animals = "catdogfrog"

# The first three characters of animals
cat = animals[:3]
# The fourth through sixth characters
dog = animals[3:6]
# From the seventh character to the end
frog = animals[6:]

print cat, dog , frog

#Ejercicio 3

start_list = [5, 3, 1, 2, 4]
square_list = []

# Your code here!
for x in start_list:
  
  square_list.append(x ** 2)
  square_list.sort()

print square_list

#Ejercicio 4

inventory = {
  'gold' : 500,
  'pouch' : ['flint', 'twine', 'gemstone'],
  'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
}

inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

inventory['pouch'].sort() 

# Your code here
inventory['pocket'] = ['seashell', 'strange berry', 'lint']
inventory['backpack'].sort()
inventory['backpack'].remove('dagger')
inventory['gold'] = inventory['gold'] + 50

print inventory

##Ejercicio 5

prices = {
  "banana": 4,"apple": 2,"orange": 1.5,"pear": 3
				}

stock = {
  "banana": 6, "apple": 0, "orange": 32, "pear": 15
				}

total = 0
for x in prices:
  print x
  print "price: %s" % prices[x]
  print "stock: %s" % stock[x]
  print prices[x] * stock[x]
  print
  total = total + prices[x] * stock[x]
print total

#Ejercicio 6

shopping_list = ["banana", "orange", "apple"]

stock = {
  "banana": 6,
  "apple": 0,
  "orange": 32,
  "pear": 15
}
    
prices = {
  "banana": 4,
  "apple": 2,
  "orange": 1.5,
  "pear": 3
}

# Write your code below!
def compute_bill(food):
  total = 0
  for item in food:
    if stock[item] > 0:
      total = total + prices[item]
      stock[item] -= 1
      print stock[item]
  return total

print compute_bill(shopping_list)


#### Lección 6 

#Ejercicio 1

lloyd = {
  "name": "Lloyd",
  "homework": [90.0, 97.0, 75.0, 92.0],
  "quizzes": [88.0, 40.0, 94.0],
  "tests": [75.0, 90.0]
}
alice = {
  "name": "Alice",
  "homework": [100.0, 92.0, 98.0, 100.0],
  "quizzes": [82.0, 83.0, 91.0],
  "tests": [89.0, 97.0]
}
tyler = {
  "name": "Tyler",
  "homework": [0.0, 87.0, 75.0, 22.0],
  "quizzes": [0.0, 75.0, 78.0],
  "tests": [100.0, 100.0]
}

students = [lloyd, alice, tyler]

for student in students:
  print "Name: %s" % student["name"]
  print "Homework: %s" % student["homework"]
  print "Quizz: %s" % student["quizzes"]
  print "Test: %s" % student["tests"]
  print

# Add your function below!
def average(numbers):
  total = sum(numbers)
  total = float(total)
  return total/len(numbers)

def get_average(student):
  homework = average(student["homework"])
  quizzes = average(student["quizzes"])
  tests = average(student["tests"])
  return 0.1 * homework + 0.3 * quizzes + 0.6 * tests

def get_letter_grade(score):
  if score >= 90:
    return "A"
  elif score >=80:
    return "B"
  elif score >=70:
    return "C"
  elif score >=60:
    return "D"
  else:
    return "F"
  
print "name: Lloyd, Nota: " + get_letter_grade(get_average(lloyd))
print "name: Alice, Nota: " + get_letter_grade(get_average(alice))
print "name: Tyler, Nota: " + get_letter_grade(get_average(tyler))
print 

def get_class_average(class_list):
  results = []
  for student in class_list:
    student_avg = get_average(student)
    results.append(student_avg)
  return average(results)

print "Promedio general de la clase"
print get_class_average(students)
print get_letter_grade(get_class_average(students))