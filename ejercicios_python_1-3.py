## -*- coding: utf-8 -*-
#Ejercicios_python_1_3.py

###Lección 1

#Ejercicio 1 Variables

monty = True
python = 1.234
monty_python = python ** 2

print monty
print python
print monty_python

#Ejercicio 

meal = 44.50
tax = 6.75 / 100
tip = 15.0 / 100

meal = meal + meal * tax
total = meal + meal * tip

print tip
print("%.2f" % total)

###Lección 2

#Ejercicio1 Metodos de String, imprimir en consola

my_string = "Hola Mundo"
print len(my_string)
print my_string.upper()

#Ejercicio2 Datetime

from datetime import datetime
now = datetime.now()

print '%s/%s/%s %s:%s:%s' % (now.month, now.day, now.year, now.hour, now.minute, now.second)

###Lección 3

#Ejercicio1 Condicionales 

def grade_converter(grade):
    if grade >= 90:
        return "A"
    elif grade >= 80:
        return "B"
    elif grade >= 70:
        return "C"
    elif grade >= 65:
        return "D"
    else:
        return "F"
      
# This should print an "A"      
print grade_converter(92)

# This should print a "C"
print grade_converter(70)

# This should print an "F"
print grade_converter(61)

#Ejercicio 2 Pyglatin

pyg = 'ay'

original = raw_input('Enter a word:')

if len(original) > 0 and original.isalpha():
  word = original.lower()
  first = word[0]
  new_word = word + first + pyg
  new_word = new_word[1:len(new_word)]
  print new_word
else:
    print 'empty'



